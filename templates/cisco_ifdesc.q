exports.query = {
	tv: 1,
	description: 'cisco mac addresses',
	oid: '.1.3.6.1.4.1.9.2.2.1.1.28',
	prepare: function(input){
		var output = {};
		for (var one in input){
			var p = one.split(',');
			output['port '+(p[p.length-1])+' description'] = input[one];
		}
		return output;
	}
}