exports.query = {
	tv: 1,
	description: 'cisco port errors',
	oid: '.1.3.6.1.2.1.2.2.1.14',
	prepare: function(input){
		var output = {};
		for (var one in input){
			var p = one.split(',');
			output[(p[p.length-1])] = input[one];
		}
		return output;
	}
}