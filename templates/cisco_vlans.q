exports.query = {
	tv: 1,
	description: 'vlans for 2950',
	oid: '.1.3.6.1.4.1.9.9.68.1.2.1.1.2',
	prepare: function(input){
		var ports = {};
		var output = {};

		function addToPort(port, vlan){
			if (ports[port] === undefined)
				ports[port] = [];
			ports[port].push(vlan);
		}

		for (var one in input){
			var tmp = one.split(',');
			for (var i = 0; i < input[one].length; i++)
				for (var pos = 7; pos >= 0; pos--)
					if (((input[one].charCodeAt(i)>>pos)&1)==1)
						addToPort(8*i+(8-pos),tmp[tmp.length-1]);
		}

		for (var one in ports){
			output['port '+one] = 'vlan: '+ports[one];
		}
		
		return output;
	}
}