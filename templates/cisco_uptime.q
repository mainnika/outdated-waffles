exports.query = {
	tv: 1,
	description: 'cisco uptime',
	oid: '.1.3.6.1.2.1.1.3',
	prepare: function(input){
		var output = {};
		for (var one in input){
			var h = Math.round(input[one]/360000),
				m = Math.round((input[one]%360000)/6000),
				s = Math.round(((input[one]%360000)%6000)/100);
			output['uptime'] = ((h>0)?(h+' hours '):'')+((m>0)?(m+' minutes '):'')+((s>0)?(s+' seconds'):'');
		}
		return output;
	}
}