exports.query = {
	tv: 1,
	description: 'cisco mac addresses',
	oid: '.1.3.6.1.4.1.9.9.87.1.4.1.1.32',
	prepare: function(input){
		var output = {};
		for (var one in input){
			var p = one.split(',');
			output['port '+(p[p.length-1])+' duplex'] = input[one]==1?'full':'half';
		}
		return output;
	}
}