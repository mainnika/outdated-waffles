exports.query = {
	tv: 1,
	description: 'cisco mac addresses',
	oid: '.1.3.6.1.4.1.9.9.46.1.6.1.1.14',
	prepare: function(input){
		var output = {};
		for (var one in input){
			var p = one.split(',');
			output['port '+(p[p.length-1])+' type'] = input[one]==1?'trunc':'access';
		}
		return output;
	}
}