exports.query = {
	tv: 1,
	description: 'cisco management ip',
	oid: '.1.3.6.1.2.1.4.20.1.1',
	prepare: function(input){
		var output = {},
			c = 0;
		for (var one in input){
			output['ip '+(++c)] = input[one];
		}
		return output;
	}
}