exports.query = {
	tv: 1,
	description: 'cisco mac addresses',
	oid: '.1.3.6.1.2.1.2.2.1.6',
	prepare: function(input){
		var output = {};
		for (var one in input){
			var p = one.split(','),
				mac = '';
			for (var i = 0; i < 6; i++)
				mac+=input[one].charCodeAt(i).toString(16)+((i!=5)?':':'');
			output['port '+(p[p.length-1])+' mac'] = mac;
		}
		return output;
	}
}