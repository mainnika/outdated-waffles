var async = require('async'),
	log = require('./logger.js').getLogger('CRON');

function SimpleCron(){
	this._tasks = {}; // {u: {f: F, t: 30}, ...}
	this._running = {}; // {u: timer_id, ...}
}

SimpleCron.prototype = (function(){

	function wrapper(uniq, func, args, ctx){
		log.info('Execute '+uniq);
		func.apply(ctx, args);
	}

	function success(err,result){

		if (err)
			return log.info('Cron function failed: '+err);

		this.storage.storeSnmpQueryResult(result,function(err,result){
			if (err)
				return log.info('Saving cron result failed');

			log.info('Saving success, id: '+result);
		})

	}

	return {
		'add': function(uniq, func, args, ctx, timing, run){

			if ((typeof func != 'function')||(typeof timing != 'number')||(this._tasks[uniq] !== undefined))
				return;

			log.info('Adding '+uniq+', at '+timing+' sec');

			this._tasks[uniq] = {f: function(){wrapper(uniq,func,args,ctx)}, t: timing};
			if (run)
				this.run(uniq);
		},
		'rm': function(uniq){
			this.stop(uniq);
			this._tasks[uniq] = undefined;
		},
		'clear': function(){
			for (var i in this._tasks)
				this.rm(i);
		},
		'size': function(){
			return (function(){
					var size = 0, key;
					for (key in this._tasks)
						if (this._tasks.hasOwnProperty(key)) size++;
					return size;
			})();
		},
		'run': function(uniq){
			log.info('Enable '+uniq);
			if (this._tasks[uniq])
				this._running[uniq] = setInterval(this._tasks[uniq].f,this._tasks[uniq].t*1000);
		},
		'instant': function(uniq){
			log.info('Instant cron query: '+uniq);
			if (this._tasks[uniq])
				this._tasks[uniq].f();
		},
		'stop': function(uniq){
			log.info('Disable '+uniq);
			clearInterval(this._running[uniq]);
		},
		'reset': function(callback){
			

		}
	}
})();

exports.getSimpleCron = function(callback){
	log.info('Getting DB for SimpleCron');
	require('./sql_storage.js').getStorage(function(err,storage){
		log.info('DB Storage '+((err)?err:storage));
		callback((err)?err:null,(err)?null:(new SimpleCron(storage)));
	})
}