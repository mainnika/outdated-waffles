var winston = require('winston');

function Logger(module, _loginstance){
	this._module = module;
	this._logger = _loginstance;
}

Logger.prototype = (function(){

	return {
		info: function(msg){
			this._logger.info('/'+this._module+'/ '+msg);
		},
		err: function(msg){
			this._logger.error('/'+this._module+'/ '+msg);
		}
	}
})()

Logger._instance = null;

exports.getLogger = (function(){

	if (!Logger._instance){
		Logger._instance = new (winston.Logger)({
			transports: [
				//new (winston.transports.File)({timestamp: true, filename: '/var/log/waffles.log', json: false}),
				new (winston.transports.Console)({timestamp: true, colorize: false})
			]
		});
	}

	return function(module){
		return (new Logger(module, Logger._instance));
	}

})();