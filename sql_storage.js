/**
 * Waffles MySQL Storage
 * mainnika, 2013
 */

var mysql = require('mysql'),
	async = require('async'),
	conf = require('./mysql.conf').sql,
	msgbus = require('./msgbus.js'),
	log = require('./logger.js').getLogger('STORAGE');

function SQLStorage(callback) {

	var self = this;

	(this._reconnect = function(err, first){
		if (err){
			self._ready = false;
			self._db = null;
			log.err('DB connection crashed: '+err);
		}
		log.info('Getting new connection...');

		SQLStorage._instance.getConnection(function(err,connection){

			if (err)
				return setTimeout(function(){
					log.info('Waiting...'); self._reconnect(err, first)
				}, 500);

			log.info('There\'s a new SQL connection: '+connection);
			self._db = connection;
			self._db.on('error', self._reconnect);
			self._ready = true;

			if (first)
				callback(null, self);
		})
	})(null, true);

}

SQLStorage.prototype = (function() {

	return {

		'_': function(callback){
			if (!this._ready){
				log.info('Db not ready');
				callback({error: 'Db not ready'},null);
				return true;
			}
			return false;
		},

		// DEVICES //
		'getAllDevices': function(callback) {
			log.info('Querying getDevices');
			if (this._(callback)) // DB connection
				return;
			this._db.query('select \
				`d`.`device_id` as `_id`, \
				`s`.`value` as `name`, \
				`d`.`image`, \
				`d`.`ip`, \
				`d`.`community`, \
				`d`.`comment`, \
				`d`.`x`, \
				`d`.`y`, \
				`d`.`name_x`, \
				`d`.`name_y` \
				from \
				( \
					SELECT \
					`devices`.`_id` AS `device_id`, \
					`devices`.`name_query` as `name_id`, \
					max(`snmp_storage`.`_id`) as `storage_id`, \
					`devices`.`image`, \
					`devices`.`ip`, \
					`devices`.`community`, \
					`devices`.`comment`, \
					`map`.`x`, `map`.`y`, `map`.`name_x`, `map`.`name_y` \
					FROM `devices` \
					LEFT JOIN (`map`, `snmp_storage`) \
					ON (`map`.`device_id` =  `devices`.`_id` and `snmp_storage`.`function_id` = `devices`.`name_query` and `snmp_storage`.`device_id` = `devices`.`_id`) \
					group by `snmp_storage`.`device_id` \
				) as `d` \
				left join `snmp_storage_str` as `s` \
				on (`s`.`storage_id` = `d`.`storage_id`)', 
				[], function(err,results){
					log.info('getDevices query status: [error: "'+err+'", ok: "'+((results)?results.length:null)+'"]');
					callback((err)?err:null,(err)?null:results);
				}
			)
		},
		'getDeviceById' : function(id, callback) {
			log.info('Querying getDeviceById');
			if (this._(callback)) // DB connection
				return;
			this._db.query('select `devices`.`_id`, `devices`.`name_query`, `devices`.`community`, `devices`.`comment`, `devices`.`ip`, `devices`.`image`, `snmp_queries`.`_id` as `query_id`, `snmp_queries`.`snmp_function_id` from `devices`, `snmp_queries` where `devices`.`_id` = `snmp_queries`.`device_id` and `devices`.`_id` = ?', [id], function(err,results){
				log.info('getDeviceById query status: [error: "'+err+'", ok: "'+((results)?results.length:null)+'"]');
				callback((err)?err:null,(err)?null:results);
			})
		},
		'updateDevice': function(device_id, ip, community, image, comment, callback){
			log.info('Updating device');
			if (this._(callback))
				return;
			var self = this;

			if ((!ip)&&(!community)&&(!image)&&(!comment))
				return callback('no fields for change');
			
			async.parallel({
				'image': function(callback){
					if (image){
						self._db.query('update `devices` set `image` = ? where `_id` = ?', [image,device_id], function(err,result){
							log.info('Image changed');
							callback(null,(err)?err:"ok");
						});
					}else{
						callback();
					}
				},
				'ip': function(callback){
					if (ip){
						self._db.query('update `devices` set `ip` = ? where `_id` = ?', [ip,device_id], function(err,result){
							log.info('Ip changed');
							callback(null,(err)?err:"ok");
						});
					}else{
						callback();
					}
				},
				'community': function(callback){
					if (community){
						self._db.query('update `devices` set `community` = ? where `_id` = ?', [community,device_id], function(err,result){
							log.info('Community changed');
							callback(null,(err)?err:"ok");
						});
					}else{
						callback();
					}
				},
				'comment': function(callback){
					if (comment){
						self._db.query('update `devices` set `comment` = ? where `_id` = ?', [comment,device_id], function(err,result){
							log.info('Comment changed');
							callback(null,(err)?err:"ok");
						});
					}else{
						callback();
					}
				}
			}, function(err,result){
				var output = {};
				if (result.image)
					output.image_changed = result.image;
				if (result.ip)
					output.ip_changed = result.ip;
				if (result.community)
					output.community_changed = result.community;
				if (result.comment)
					output.comment_changed = result.comment;
				callback(null,output);
			})
		},
		'addDeviceQueries': function(device_id, queries, callback){
			log.info('Adding queries for device');
			if (this._(callback))
				return;
			var prepared = [];
			for (var i in queries){
				prepared.push([device_id, queries[i].snmp_function_id, queries[i].interval]);
			}
			this._db.query('insert into `snmp_queries` (`device_id`, `snmp_function_id`, `interval`) values ?', [prepared], function(err,result){
				
				msgbus.emit('cron_reload', function(err,result){log.info('New cron size: '+result)});

				callback(((err)?err:null),((err)?null:"ok"));
			})
		},
		'unmapDevice': function(device_id, callback){
			log.info('Unmapping device');
			if (this._(callback))
				return;
			this._db.query('delete from `map` where `device_id` = ?', [device_id], function(err,result){
				callback((err)?err:null,(err)?null:"ok");
			})
		},
		'mapDevice': function(device_id, x, y, callback){
			log.info('Mapping device '+device_id);
			var self = this;
			if (this._(callback))
				return;
			this.unmapDevice(device_id,function(){
				self._db.query('insert into `map` (`device_id`, `x`, `y`) values (?)',[[device_id, x, y]], function(err,result){
					callback(((err)?err:null),((err)?null:"ok"));
				})
			})
		},
		'addDevice': function(ip, community, comment, template_id, queries, x, y, callback){
			var self = this;
			log.info('Inserting new device '+ip);
			if (this._(callback))
				return;
			self.getTemplateById(template_id, function(err,template){
				if (err)
					return callback(err);
				if (template.length < 1)
					return callback('no suck template');

				self._db.query('insert into `devices` (`community`,`ip`) values (?)', [[community,ip]], function(err,result){
					if (err)
						return callback(err);
					log.info('New device ID: '+result.insertId);
					var device_id = result.insertId;

					async.parallel({
						'setImage': function(callback){
							self.updateDevice(device_id, undefined, undefined, template[0].image, comment, callback);
						},
						'setQueries': function(callback){
							self.addDeviceQueries(device_id, template, callback);
						},
						'setAdditionalQueries': function(callback){
							if (queries.length>0){

								var prepared = [];
								for (var i in queries){
									prepared.push({snmp_function_id: 0|queries[i], interval: 0});
								}

								self.addDeviceQueries(device_id, prepared, callback);
							}else{
								callback();
							}
						},
						'mapDevice': function(callback){
							if ((x)&&(y)){
								self.mapDevice(device_id, x, y, callback);
							}else{
								callback();
							}
						}
					},
					function(err,results){
						callback(((err)?err:null),((err)?null:{device_id: device_id}));
					})
				})
			})
		},
		'removeDevice': function(device_id,callback){
			log.info('Removing device '+device_id);
			if (this._(callback))
				return;
			this._db.query('delete from `devices` where `_id` = ?',[device_id],function(err,result){
				if (err)
					return callback(err,null);
				log.info('Ok');
				callback(null,'ok');
			})
		},
		'moveDevice': function(device,x,y,callback){
			log.info('Updating device XY...');
			if (this._(callback)) // DB connection
				return;
			this._db.query('update `map` set `x` = ?, `y` = ? where `device_id` = ?',[x,y,device],function(err,result){
				if (err)
					return callback(err,null);
				log.info('XY Updated');
				callback(null,"ok");
			})
		},
		'moveName': function(device,x,y,callback){
			log.info('Updating device name XY...');
			if (this._(callback)) // DB connection
				return;
			this._db.query('update `map` set `name_x` = ?, `name_y` = ? where `device_id` = ?',[x,y,device],function(err,result){
				if (err)
					return callback(err,null);
				log.info('Name XY Updated');
				callback(null,"ok");
			})
		},

		// LINKS //
		'getLinks': function(callback) {
			log.info('Getting links');
			if (this._(callback)) // DB connection
				return;
			this._db.query('select `_id`, `one`, `two`, `one_port`, `two_port`, `one_query`, `two_query` from `map_links`', [], function(err,results){
				log.info('getLinks query status: [error: "'+err+'", ok: "'+(results)?results.length:null+'"]');
				callback((err)?err:null,(err)?null:results);
			})
		},
		'updatePolylineData': function(link_id, data, callback){
			log.info('Updating polyline: '+link_id);
			// if (this._(callback)) // DB connection
			// 	return;
			callback("not implemented");
			// var self = this;

			// // if point id then updating xy only
			// if (point_id){
			// 	return self._db.query('update `map_polylines` set `x` = ?, `y` = ? where `_id` = ?', [x,y,point_id], callback);
			// }

			// if (link_id){
				
			// }

			// async.waterfall([

			// 	function(callback){
			// 		self._db.query('select `_id` from `map_polylines` where `_id` = ? and `link_id` = ? and `next_id` = ?',[point_id, link_id, next_id], function(err,result){
			// 			callback(err,(result.length>0));
			// 		})
			// 	},

			// 	function()

			// ])
		},
		'getLinkById': function(link_id, callback){
			log.info('Getting link: '+link_id);
			if (this._(callback)) // DB connection
				return;
			this._db.query('select `one`, `two`, `one_port`, `two_port`, `one_query`, `two_query`  from `map_links` where `_id` = ?', [link_id], function(err,result){
//				log.info('getLinks query status: [error: "'+err+'", ok: "'+(results)?results.length:null+'"]');
				callback((err)?err:null,(err)?null:result[0]);
			})
		},
		'addLink': function(device_one, device_two, port_one, port_two, callback){
			log.info('Adding link');
			if (this._(callback)) // DB connection
				return;
			this._db.query('insert into `map_links` (`one`,`two`,`one_port`,`two_port`) values (?) ', [[device_one, device_two, port_one, port_two]], function(err,result){
				log.info('Link added');
				callback((err)?err:null,(err)?null:{'link_id': result.insertId});
			})			
		},
		'rmLink': function(link_id, callback){
			log.info('Removing link '+link_id);
			if (this._(callback)) // DB connection
				return;
			this._db.query('delete from `map_links` where `_id` = ?',[link_id],function(err,result){
				log.info('Link '+link_id+' removed');
				callback((err)?err:null,(err)?null:result);
			})
		},
		'updateLink': function(link_id, one_query, two_query, one_port, two_port, callback){
			log.info('Updating link '+link_id);
			var self = this;
			if (this._(callback)) // DB connection
				return;
			
			if ((one_query==0)&&(two_query==0)&&(!one_port)&&(!two_port))
				return callback('no fields for change');

			async.parallel({
				'one_query': function(callback){
					if (one_query!=0){
						self._db.query('update `map_links` set `one_query` = ? where `_id` = ?',[one_query, link_id], function(err,result){
							log.info('Query one changed');
							callback(null, (err)?err:"ok");
						})
					}else{
						callback();
					}
				},
				'two_query': function(callback){
					if (two_query!=0){
						self._db.query('update `map_links` set `two_query` = ? where `_id` = ?',[two_query, link_id], function(err,result){
							log.info('Query two changed');
							callback(null, (err)?err:"ok");
						})
					}else{
						callback();
					}
				},
				'one_port': function(callback){
					if (one_port){
						self._db.query('update `map_links` set `one_port` = ? where `_id` = ?',[one_port, link_id], function(err,result){
							log.info('Port one changed');
							callback(null, (err)?err:"ok");
						})
					}else{
						callback();
					}
				},
				'two_port': function(callback){
					if (two_port){
						self._db.query('update `map_links` set `two_port` = ? where `_id` = ?',[two_port, link_id], function(err,result){
							log.info('Port two changed');
							callback(null, (err)?err:"ok");
						})
					}else{
						callback();
					}
				}
			}, function(err,result){
				var output = {};
				if (result.one_query)
					output.query_one_changed = result.one_query;
				if (result.two_query)
					output.query_two_changed = result.two_query;
				if (result.one_port)
					output.port_one_changed = result.one_port;
				if (result.two_port)
					output.port_two_changed = result.two_port;
				callback(null,output);
			})

		},
		'getLinkStatuses': function(callback){
			log.info('Selecting link statuses...');

			if (this._(callback)) // DB connection
				return;

			this._db.query('select l.link_id, l.device_id, l.port, `int`.value as `status`\
				from\
				(\
					select l.link_id, l.device_id, l.port, l.status_func, l.name_func, l.status_id, max(s2._id) as name_id\
					from\
					(\
						select l.link_id, l.uuid, l.device_id, l.port, l.status_func, l.name_func, max(s1._id) as status_id\
						from\
						(\
							select _id as link_id, rand() as uuid, one as device_id, one_port as port, one_query as status_func, one_name_func as name_func\
							from map_links\
							union \
							select _id, rand(), two, two_port, two_query, two_name_func\
							from map_links\
						) as l\
						left join snmp_storage as s1\
						on (s1.function_id = l.status_func and s1.device_id = l.device_id)\
						group by l.uuid\
					) as l\
					left join snmp_storage as s2\
					on (s2.function_id = l.name_func and s2.device_id = l.device_id)\
					group by l.uuid\
				) as l\
				left join snmp_storage_str as `str`\
				on (`str`.storage_id = l.name_id and `str`.value = l.port)\
				left join snmp_storage_int as `int`\
				on (`int`.`storage_id` = l.status_id and `int`.hash = `str`.hash)',
				[],callback
			)
		},

		// TEMPLATES // 
		'getTemplates': function(callback){
			log.info('Getting all templates...');
			if (this._(callback))
				return;
			this._db.query('select `templates`.`_id`, `templates`.`name`, `templates`.`image`, \
				`templates_snmp`.`snmp_function_id`, `templates_snmp`.`interval` from `templates` left join `templates_snmp` \
				on `templates`.`_id` = `templates_snmp`.`template_id`', [],
				callback
			)
		},
		'getTemplateById': function(template_id, callback){
			log.info('Getting template '+ template_id);
			if (this._(callback))
				return;
			this._db.query('select `templates`.`_id`, `templates`.`name`, `templates`.`image`, \
				`templates_snmp`.`snmp_function_id`, `templates_snmp`.`interval` from `templates` left join `templates_snmp` \
				on `templates`.`_id` = `templates_snmp`.`template_id` \
				where `templates`.`_id` = ?', [template_id], 
				callback
			)
		},
		'addTemplate': function(name, image, snmp, callback){
			log.info('Adding template '+name);
			var self = this;
			if (this._(callback))
				return;
			this._db.query('insert into `templates` (`name`,`image`) values (?)', [[name,image]], function(err,result){
				if (err) 
					return callback(err,null);
				var template_id = result.insertId;
				self._db.query('insert into `templates_snmp` (`template_id`,`snmp`) values ?',
					[(function(){
						var output = []; 
						for (var i in snmp) 
							output.push([template_id, snmp[i]]); 
						return output;
					})()], 
					function(err,result){
						callback(err,(err)?null:template_id);
					}
				)
			})
		},
		'rmTemplate': function(template_id, callback){
			log.info('Removing template '+template_id);
			if (this._(callback))
				return;
			this._db.query('delete from `templates` where `_id` = ?', [template_id], function(err,result){
				callback(err,(err)?null:"ok");
			})
		},

		// SNMP Functions
		'getSnmpFunctions': function(callback){
			log.info('Getting snmp functions...');
			if (this._(callback))
				return;
			this._db.query('select `_id`, `description`, `oid`, `extra`, `is_hex` from `snmp_functions`', [], callback);
		},
		'addSnmpFunction': function(description, oid, extra, ishex, callback){
			log.info('Adding snmp function');
			if (this._(callback))
				return;
			this._db.query('insert into `snmp_functions` (`description`, `oid`, `extra`, `is_hex`) values (?)',[[description, oid, extra || '', ishex]],function(err,result){
				callback(err,(err)?null:{function_id:result.insertId});
			})
		},

		// SNMP Queries
		'getSnmpQueryById': function(query_id, callback){
			log.info('Querying getSnmpQueryById');
			if (this._(callback)) // DB connection
				return;
			this._db.query('SELECT `snmp_queries`.`_id` as `query_id`, `devices`.`_id` as `device_id`, \
				`devices`.`ip` as `device_ip`, `devices`.`community` as `device_community`, `snmp_functions`.`oid` as `snmp_oid`, \
				`snmp_functions`.`_id` as `function_id` FROM `snmp_queries`, `devices`, `snmp_functions` \
				where `snmp_queries`.`device_id` = `devices`.`_id` and \
				`snmp_queries`.`snmp_function_id` = `snmp_functions`.`_id` and `snmp_queries`.`_id` = ?',
				[query_id], function(err,result){
					callback(err,(err)?null:result[0]);
				});
		},
		'getSnmpQueriesPeriodic': function(callback){
			log.info('Querying getPeriodicQueries');
			if (this._(callback)) // DB connection
				return;
			this._db.query('SELECT `_id`, `interval` FROM `snmp_queries` where `interval` > 0',
				[], callback);
		},
		'getSnmpQueries': function(callback){
			log.info('Querying getSnmpQueries');
			if (this._(callback)) // DB connection
				return;
			this._db.query('SELECT `snmp_queries`.`_id`, `snmp_queries`.`device_id`, `snmp_queries`.`snmp_function_id`, `snmp_functions`.`description`, `snmp_queries`.`interval` FROM `snmp_queries`, `snmp_functions` where `snmp_queries`.`snmp_function_id` = `snmp_functions`.`_id`',
				[], callback);
		},
		'addSnmpQuery': function(device_id, function_id, interval, callback){
			log.info('Adding snmp query');
			if (this._(callback))
				return;
			this._db.query('insert into `snmp_queries` (`device_id`, `snmp_function_id`, `interval`) values (?)',[[device_id, function_id, interval]],function(err,result){
				callback(err,(err)?null:{query_id:result.insertId});
			})
		},
		'storeSnmpQueryResult': function(result, callback){

			var	self = this;
			if (this._(callback))
				return;

			this._db.query('insert into `snmp_storage` (`device_id`, `function_id`, `error_msg`) values (?,?,?)', [result.device_id, result.function_id, ((result.error)?result.error.toString():null) ], function(err,storage){
				if (err)
					return callback(err,null);

				var storage_id = storage.insertId;

				if (result.error)
					return log.info('New storage record id: '+storage_id+'; saving error result');

				var snmp_result = [];

				result.results.forEach(function(value){
					snmp_result.push({
						'extra': (function(){
							var hash = 0;
							// Implementing Java's String.hashCode() for Array
							value.oid.slice(result.oid.length).forEach(function(value, index, array){
								hash+=value*Math.pow(31,array.length-1-index); 
							})
							return hash;
						})(),
						'result': value
					})
				});

				var s_int = [],
					s_str = [];

				log.info('New storage record id: '+storage_id+'; saving '+snmp_result.length+' results');

				snmp_result.forEach(function(value){

					switch(value.result.type){
						case 0x42: // Gauge
						case 0x41: // Counter
						case 0x46: // Counter64
						case 0x43: // TimeTicks
						case 0x02: // Integer
							{
								s_int.push([storage_id, value.extra, value.result.value]);
							}; break;

						case 0x04: // OctetString
						case 0x40: // IP
							{
								s_str.push([storage_id, value.extra, value.result.value]);
							}; break;
						default:
							{
								log.info("Unknow snmp type");
							}; break;
					}
				
				})

				async.parallel({
					'ints': function(callback){
						if (s_int.length>0)
							return self._db.query('insert into `snmp_storage_int` (`storage_id`, `hash`, `value`) values ?', [s_int], callback);
						callback();
					},
					'strs': function(callback){
						if (s_str.length>0)
							return self._db.query('insert into `snmp_storage_str` (`storage_id`, `hash`, `value`) values ?', [s_str], callback);
						callback();
					}
				},
				function(err,result){
					log.info((err)?'Failed to insert query':('Query inserted: '+storage_id));
					callback(err,(err)?null:storage_id);
				})

			})

		},
		'getSnmpLatestByAlias': function(device, alias, callback){

			var	self = this;
			if (this._(callback))
				return;
			//return callback("not implemented");
			log.info('Getting results for alias '+alias);

			this._db.query('select l.device_id, `str`.value as `left`, `int`.value as `right`\
				from\
				(\
					select l.device_id, l.uuid, l.left, l.right, left_id, max(s2._id) as right_id\
					from\
					(\
						select l.device_id, l.uuid, l.left, l.right, max(s1._id) as left_id\
						from\
						(\
							select `d`.`_id` as `device_id`, rand() as `uuid`, `a`.`left`, `a`.`right`\
							from `devices` as `d`\
							left join\
							(\
								select `alias`, `device_id`, `left`, `right`\
								from `snmp_functions_aliases`\
								where `alias` = ?\
								order by `device_id` desc\
							) as `a`\
							on (`a`.`device_id` is null or `a`.`device_id` = `d`.`_id` )\
							'+((device)?('	where `d`.`_id` = '+(device|0)):''/* Hard Code! */)+'\
							group by `d`.`_id`\
						) as l\
						left join snmp_storage as s1\
						on (s1.function_id = l.left and s1.device_id = l.device_id)\
						group by l.uuid\
					) as l\
					left join snmp_storage as s2\
					on (s2.function_id = l.right and s2.device_id = l.device_id)\
					group by l.uuid\
				) as l\
				left join snmp_storage_str as `str`\
				on (`str`.storage_id = l.left_id)\
				left join snmp_storage_int as `int`\
				on (`int`.`storage_id` = l.right_id and `int`.hash = `str`.hash)',[alias],function(err,results){
					if (err)
						return callback(err);
					
					log.info('Successfully getted alias');

					callback(null,results);
				}
			)
		},
		'getPortStatusBy': function(){

		},

		'staticStore': function(tag, key, type, value, callback){

			log.info('Storing static file: '+tag+':'+key);
			if (this._(callback))
				return;

			this._db.query('insert into `static_storage` (`tag`,`key`,`type`,`value`) values (?,?,?,?) ON DUPLICATE KEY UPDATE `value` = ?, `type` = ?',[tag,key,type,value,value,type],function(err,result){
				if (err)
					return callback(err);
				log.info('Successfully stored');
				callback(null,result.insertId);
			})
		},
		'staticGet': function(tag, key, callback){

			log.info('Getting static file: '+tag+':'+key);
			if (this._(callback))
				return;

			this._db.query('select `type`, `value` from `static_storage` where `tag` = ? and `key` = ? limit 1',[tag,key], function(err,result){
				if (err)
					return callback(err);
				log.info('Successfully getted');
				callback(null,result[0]);
			})	
		},
		'staticListByTag': function(tag, callback){

			log.info('Getting static list by tag: '+tag);
			if (this._(callback))
				return;

			this._db.query('select `key`, `type` from `static_storage` where `tag` = ?', [tag], function(err,result){
				if (err)
					return callback(err);
				callback(null,result);
			})

		},



		// SERVICE //
		'close': function(){
			log.info('Destroying database');
			this._db.destroy();
		},
		'getDatabase': function() {
			return this._db;
		}
	}
})();

SQLStorage._instance = null;


exports.getStorage = (function() {
	if(SQLStorage._instance === null) {
		log.info('Creating DB pool (host: '+(conf.socketPath || conf.host)+')');
		SQLStorage._instance = mysql.createPool({
			host: conf.host,
			socketPath: conf.socketPath,
			database: conf.database,
			multipleStatements: true,
			user: conf.user,
			// debug: true,
			password: conf.password
		});
	}
	return function(callback){
		// SQLStorage._instance.getConnection(function(err,connection){
		// 	log.info('New connection to database is '+((err)?'not estabilished':'created'));
		// 	if (err)
		// 		return callback(err,null);
		// 	callback(null,new SQLStorage(connection));
		// })
		new SQLStorage(callback);
	};
})();