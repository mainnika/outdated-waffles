// Message Bus

var events = require('events'),
	util = require('util'),
	log = require('./logger.js').getLogger('MSG BUS');

function MsgBus(){
	events.EventEmitter.call(this);
}

util.inherits(MsgBus, events.EventEmitter);

if (!MsgBus.__instance)
	MsgBus.__instance = new MsgBus();

exports.emit = function(event_name, argument){
	log.info('Calling event: '+event_name);
	MsgBus.__instance.emit(event_name, argument);
}

exports.on = function(event_name, handler){
	log.info('Add event: '+event_name);
	MsgBus.__instance.on(event_name, handler);
}