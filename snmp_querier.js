/**
 * Waffles snmp querier.
 * mainnika, 2013
 */

// Libraries
var async = require('async'),
	snmp = require('snmp-native'),
	zlib = require('zlib'),
	log = require('./logger.js').getLogger('SNMP');

// Constructor
function SnmpQuerier(storage){
	this._db = storage;
}

// Class implementation
SnmpQuerier.prototype = (function(){

	// Public functions
	return {

		/* Exec a query for device
		 * device sample:
		 	{
		 		_id: 2,
				ip: 'a.b.c.d',
				community: 'public'
		 	}
		 * query sample:
		 	{
		 		_id: 3,
				oid: '1.2.3.4.5.6.7.8.9.1',
				extra: ''
		 	}
		 * callback function:
		 	function(err,result)
		 */
		'queryDeviceWithQuery': function(device, query, callback){

			log.info('Execute query '+query._id+' for device '+device._id);

			var session = new snmp.Session({ host: device.ip, port: 161, community: device.community, timeouts: [1000]});
			async.parallel({
				main: function(callback){
					log.info('OID: '+query.oid);
					session.getSubtree({oid: query.oid}, callback);
				}
			},
			function(error,result){
				session.close();

				log.info('Query ok');
				
				var output = {
						device_id : device._id,
						query_id: query._id,
						function_id: query.function_id,
						oid: snmp.parseOidString(query.oid),
						//e_oid: ((query.extra!='')?snmp.parseOidString(query.extra):[]),
						//is_hex: query.is_hex,
						error: error,
						results: result.main || []
						//extra: result.extra || []
					};

				return callback(null, output);

			});

		},

		/*
		 * Executing query by id
		 */
		'queryById': function(query_id, callback){
			var self = this;

			log.info('Executing query id '+query_id);

			async.waterfall([
				function(callback){
					self._db.getSnmpQueryById(query_id, callback);
				},
				function(query, callback){
					if (query === undefined)
						return callback('No such query');
					self.queryDeviceWithQuery({
							_id: query.device_id,
							ip: query.device_ip,
							community: query.device_community
						},
						{
							_id: query.query_id,
							oid: query.snmp_oid,
							function_id: query.function_id
						},
						callback
					)
				}
			]
			, callback);
		},

		'close': function(){
			// WAT??
		}
	}
})();

exports.getSnmpQuerier = function(callback){
	log.info('Getting DB for SnmpQuerier');
	require('./sql_storage.js').getStorage(function(err,storage){
		log.info('DB Storage '+((err)?err:storage));
		callback((err)?err:null,(err)?null:(new SnmpQuerier(storage)));
	})
}