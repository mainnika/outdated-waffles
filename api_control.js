/**
 * Waffles API Controller
 * mainnika, 2013
 */

var async = require('async'),
	msgbus = require('./msgbus.js'),
	log = require('./logger.js').getLogger('API');

function parse(___){          // i love afeena
	var _ = {};               // TODO: rewrite
	for (var ____ in ___){                                 // hey, js!
		var __ = ___[____].split(':');                     // what are you doing?
		_[__.shift()]=__.join(':')||'';                    // hahahaha!!
	} return _;                                            // stop it plz;
};

function ApiController(db, snmp){

	var self = this;

	this._db = db;
	this._snmp = snmp;

	this.api = {
		'map': function(param, callback){
			log.info('Function \'map\' executing...');
			async.parallel({
				'devices': function(callback){
					self._db.getAllDevices(function(err, devices){
						callback(err,devices);
					})
				},
				'links': function(callback){
					self._db.getLinks(function(err, links){
						callback(err,links);
					})
				}
			}
			, function(err,result){
				if (err){
					log.info('Function \'map\' failed: '+err);
					callback({'error:': err});
				}else{
					log.info('Function \'map\' success');
					callback({'devices': result.devices, 'links': result.links});
				}
			})
		},

		// DEVICES //
		'get_device': function(param, callback){

			log.info('Function \'get_device\' executing...');

			self._db.getDeviceById(0|param,function(err,results){
				

				if (err)
					return callback({err: err});

				var fix = {};
				for (var i in results){
					var j = results[i];
					if (fix[j._id] === undefined){
						fix[j._id] = {_id: j._id, name: j.name, ip: j.ip, community: j.community, comment: j.comment, image: j.image, snmp: []};
					}
					fix[j._id].snmp.push({query_id: j.query_id, function_id: j.snmp_function_id});
				}

				log.info('Ok');

				callback((function(){
					var output = [];
					for (var i in fix)
						output.push(fix[i]);
					return output[0];
				})());



//				callback((err)?{error: err}:result);
			})

		},
		'update_device': function(param, callback){
			log.info('Function \'update_device\' executing...');

			var help = parse(param.split(','));

			if (!(help.device_id))
				return callback({error: 'invalid format'});

			self._db.updateDevice(help.device_id, help.ip, help.community, help.image, help.comment, function(err, result){
				callback((err)?{error: err}:result);
			});
		},
		'add_device': function(param, callback){

			log.info('Function \'add_device\' executing...');

			var help = parse(param.split(','));

			if (!(help.ip&&help.community&&help.template_id))
				return callback({error: 'invalid format'});

			self._db.addDevice(help.ip, help.community, help.comment, help.template_id, ((help.queries)?help.queries.split(';'):[]), help.x, help.y, function(err, result){
				callback(((err)?{error: err}:result));
			});
		},
		'map_device': function(param, callback){

			log.info('Function \'map_device\' executing...');

			var help = parse(param.split(','));

			if (!(help.device_id&&help.x&&help.y))
				return callback({error: 'invalid format'});

			self._db.mapDevice(help.device_id, help.x, help.y, function(err, result){
				callback((err)?{error: err}:result);
			});
		},
		'unmap_device': function(param, callback){

			log.info('Function \'unmap_device\' executing...');

			self._db.unmapDevice(0|param, function(err, result){
				callback((err)?{error: err}:result);
			});
		},
		'move_device': function(param, callback){

			log.info('Function \'pos_update\' executing...');

			var help = parse(param.split(','));

			if (!(help.device_id&&((0|help.x)==help.x)&&((0|help.y)==help.y)))
				return callback({error: 'invalid query format'});

			self._db.moveDevice(help.device_id,help.x,help.y,function(err,result){
				callback((err)?{error: err}:result);
			});

		},
		'move_name': function(param, callback){

			log.info('Function \'move_name\' executing...');

			var help = parse(param.split(','));

			if (!(help.device_id&&((0|help.x)==help.x)&&((0|help.y)==help.y)))
				return callback({error: 'invalid query format'});

			self._db.moveName(help.device_id,help.x,help.y,function(err,result){
				callback((err)?{error: err}:result);
			});

		},
		'rm_device': function(param, callback){

			log.info('Function \'rm_device\' executing...');

			self._db.removeDevice(0|param, function(err,result){
				callback((err)?{error: err}:result);
			});
		},

		// LINKS //
		'add_link': function(param, callback){

			log.info('Function \'add_link\' executing...');

			var help = parse(param.split(','));
			if (!(help.one&&help.two&&help.one_port&&help.two_port))
				return callback({error: 'invalid format'});

			self._db.addLink(0|help.one, 0|help.two, help.one_port, help.two_port, function(err,result){
				callback((err)?{error: err}:result);
			})
		},
		'rm_link': function(param, callback){
			log.info('Function \'rm_link\' executing...');

			self._db.rmLink(0|param, function(err,result){
				callback((err)?{error: err}:"ok");
			})
		},
		'get_link': function(param, callback){
			log.info('Function \'rm_link\' executing...');

			self._db.getLinkById(0|param, function(err,result){
				callback((err)?{error: err}:result);
			})
		},
		'update_link': function(param, callback){
			log.info('Function \'update_link\' executing...');

			var help = parse(param.split(','));
			if (!(help.link_id))
				return callback({error: 'invalid format'});

			self._db.updateLink(help.link_id, 0|help.one_query, 0|help.two_query, help.one_port, help.two_port, function(err,result){
				callback((err)?{error: err}:result);
			})
		},
		'get_link_statuses': function(param, callback){
			log.info('Function \'get_link_statuses\' executing...');

			self._db.getLinkStatuses(function(err,results){
				callback((err)?{error: err}:results);
			})
		},

		// TEMPLATES //
		'get_templates': function(param, callback){
			log.info('Function \'get_templates\' executing...');
			self._db.getTemplates(function(err,results){

				if (err)
					return callback({err: err});

				var fix = {};
				for (var i in results){
					var j = results[i];
					if (fix[j._id] === undefined){
						fix[j._id] = {_id: j._id, name: j.name, snmp: [], image: j.image};
					}
					fix[j._id].snmp.push(j.snmp_function_id);
				}

				log.info('Ok');

				callback((function(){
					var output = [];
					for (var i in fix)
						output.push(fix[i]);
					return output;
				})());
			})
		},
		'add_template': function(param, callback){
			log.info('Function \'add_template\' executing...');
			var help = parse(param.split(','));

			if (!(help.name&&help.image&&help.snmp))
				return callback({error: 'invalid format'});

			self._db.addTemplate(help.name,help.image,help.snmp.split(';'), function(err,result){
				callback((err)?{error: err}:{template_id: result});
			});

		},
		'rm_template': function(param, callback){

			log.info('Function \'rm_template\' executing...');

			self._db.rmTemplate(0|param, function(err,result){
				callback((err)?{error: err}:result);
			})
		},

		// SNMP Functions
		'get_snmp_functions': function(param, callback){
			log.info('Function \'get_snmp_functions\' executing...');
			self._db.getSnmpFunctions(function(err,result){
				callback((err)?{error: err}:result);
			})
		},
		'add_snmp_function': function(param, callback){
			var help = parse(param.split(','));
			if (!(help.description&&help.oid&&help.ishex))
				return callback({error: 'invalid format'});
			self._db.addSnmpFunction(help.description, help.oid, help.extra, help.ishex, function(err,result){
				callback((err)?{error: err}:result);
			})
		},

		// SNMP Qu
		'get_snmp_queries': function(param, callback){
			self._db.getSnmpQueries(function(err,result){
				callback((err)?{error: err}:result);
			})
		},
		'add_snmp_query': function(param, callback){
			var help = parse(param.split(','));
			if (!(help.device_id&&help.function_id&&help.interval))
				return callback({error: 'invalid format'});
			self._db.addSnmpQuery(0|help.device_id, 0|help.function_id, 0|help.interval, function(err,result){
				callback((err)?{error: err}:result);
			})
		},
		'get_snmp_latest': function(param, callback){
			var help = parse(param.split(','));
			if (!(help.alias))
				return callback({error: 'invalid format'});
			self._db.getSnmpLatestByAlias(help.device_id, help.alias, function(err,result){
				callback((err)?{error: err}:result);
			})
		},

		'cron_reload': function(param, callback){
			msgbus.emit('cron_reload',function(err,result){
				callback((err)?{error: err}:{cron_size: result});
			});
		},


		'get_static_list': function(param, callback){
			if (!param)
				return callback({error: 'invalid format'});
			self._db.staticListByTag(param, function(err,result){
				callback((err)?{error: err}:result);
			})
		}

		// // CRON
		// 'cron_run': function(param, callback){
		// 	log.info('Function \'cron_run\' executing...');
		// 	self._cron.instant(param);
		// 	callback(null, "runned");
		// 	// self._db.getSnmpFunctions(function(err,result){
		// 	// 	callback((err)?{error: err}:result);
		// 	// })
		// }


	}
}

ApiController.prototype = (function(){
	return {
		// 'setCronEngine': function(cron){
		// 	this._cron = cron;
		// }
	}
})();

// ApiController Factory
exports.getApiController = function(callback){
	
	log.info('Getting DB for ApiController');
	require('./sql_storage.js').getStorage(function(err,storage){
		callback((err)?err:null,(err)?null:(new ApiController(storage)));
	})

};