/**
 * Waffles Core
 * mainnika, 2013
 */

var async = require('async'),
	msgbus = require('./msgbus.js'),
	log = require('./logger.js').getLogger('APP');

log.info('Starting...');

var config = {
	http_port: 8080
}

async.waterfall([

	// App Init
	function(callback){
		var app = {};
		callback(null,app)
	},

	// Storage
	function(app,callback){
		log.info('Starting DB...');
		require('./sql_storage.js').getStorage(function(err,storage){
			if (err)
				return callback(err,app);
			app.storage = storage;
			log.info('DB OK: '+storage);
			callback(null,app);
		})
	},

	// API
	function(app,callback){
		log.info('Starting API...');
		require('./api_server.js').getApiServer(config.http_port,function(err,api){
			if (err)
				return callback(err,app);
			app.api = api;
			log.info('API OK: '+api); 
			callback(null,app);
		})
	},

	// SNMP
	function(app,callback){
		log.info('Starting SNMP Queries...');
		require('./snmp_querier.js').getSnmpQuerier(function(err,snmp){
			if (err)
				return callback(err,app);
			app.snmp = snmp;
			log.info('SNMP OK: '+snmp); 
			callback(null,app);
		})
	},

	// CRON
	function(app,callback){
		log.info('Preparing CRON...');
		require('./simple_cron.js').getSimpleCron(function(err,cron){
			if (err)
				return callback(err,app);
			app.cron = cron;
			log.info('CRON OK: '+cron); 
			callback(null,app);
		})
	},

	// API Control
	function(app,callback){
		log.info('Starting ApiController...');
		new require('./api_control.js').getApiController(function(err,apicontrol){
			if (err)
				return callback(err,app);
			app.apicontrol = apicontrol;
			log.info('ApiController OK: '+apicontrol);
			callback(null,app);
		})
	}

]
, function(err,app){

	if (err)
		return log.err('Init error: '+err);

	var self = this;

	log.info('Appending api function...');
	var api = app.apicontrol.api;
	for (var cmd in api)
		app.api.pushFunc(cmd,api[cmd]);
	log.info('All function appended');

	msgbus.on('cron_reload',(function(){

		function reload(callback){
			log.info('Reloading CRON functions...');
			app.storage.getSnmpQueriesPeriodic(function(err,results){
				if (err){
					log.err('getSnmpQueriesPeriodic failed' + err);
					return callback(err);
				}

				for (i in results)
					app.cron.add(results[i]._id, app.snmp.queryById, [results[i]._id,
						function(err,result){
							if (err){
								log.info('Cron function failed: '+err);
								return;
							}

							app.storage.storeSnmpQueryResult(result,function(err,result){
								if (err){
									log.info('Saving cron result failed');
									return;
								}
								log.info('Saving success, id: '+result);
							})
						}],
						app.snmp, results[i].interval, true
					);

				callback(null,app.cron.size());
			})
		};
		reload(function(err,result){
			log.info('New cron size: '+ result);
		});

		return reload;
	})());

	log.info('APP OK!');

})