-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 29, 2013 at 06:28 PM
-- Server version: 5.1.67
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `waffles`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_query` int(11) NOT NULL DEFAULT '2',
  `community` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `name_query` (`name_query`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`_id`, `name_query`, `community`, `ip`, `image`) VALUES
(3, 2, 'v-lan.monitored.ro', '10.10.0.3', 'pic/switch.png'),
(8, 2, 'v-lan.monitored.ro', '10.10.0.16', 'pic/switch.png');

-- --------------------------------------------------------

--
-- Table structure for table `map`
--

CREATE TABLE IF NOT EXISTS `map` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `x` int(11) NOT NULL,
  `y` int(11) NOT NULL,
  `name_x` int(11) NOT NULL DEFAULT '54',
  `name_y` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=binary AUTO_INCREMENT=20 ;

--
-- Dumping data for table `map`
--

INSERT INTO `map` (`_id`, `device_id`, `x`, `y`, `name_x`, `name_y`) VALUES
(5, 8, 393, 156, 26, 59),
(6, 3, 698, 169, 23, 51);

-- --------------------------------------------------------

--
-- Table structure for table `map_links`
--

CREATE TABLE IF NOT EXISTS `map_links` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `one` int(11) NOT NULL,
  `two` int(11) NOT NULL,
  `one_port` varchar(64) CHARACTER SET utf8 NOT NULL,
  `two_port` varchar(64) CHARACTER SET utf8 NOT NULL,
  `one_query` int(11) NOT NULL DEFAULT '1',
  `two_query` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`_id`),
  KEY `one` (`one`),
  KEY `two` (`two`),
  KEY `one_query` (`one_query`),
  KEY `two_query` (`two_query`)
) ENGINE=InnoDB  DEFAULT CHARSET=binary AUTO_INCREMENT=17 ;

--
-- Dumping data for table `map_links`
--

INSERT INTO `map_links` (`_id`, `one`, `two`, `one_port`, `two_port`, `one_query`, `two_query`) VALUES
(2, 3, 8, 'Fa1/1', 'Fa0/1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `snmp_functions`
--

CREATE TABLE IF NOT EXISTS `snmp_functions` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `oid` varchar(255) NOT NULL,
  `extra` varchar(255) NOT NULL,
  `is_hex` int(11) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `snmp_functions`
--

INSERT INTO `snmp_functions` (`_id`, `description`, `oid`, `extra`, `is_hex`) VALUES
(1, 'port name: port duplex', '.1.3.6.1.2.1.10.7.2.1.19', '.1.3.6.1.2.1.31.1.1.1.1', 0),
(2, 'sys hostname', '.1.3.6.1.4.1.9.2.1.3', '', 0),
(3, 'sys uptime', '.1.3.6.1.2.1.1.3', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `snmp_queries`
--

CREATE TABLE IF NOT EXISTS `snmp_queries` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `snmp_function_id` int(11) NOT NULL,
  `interval` int(11) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `device_id` (`device_id`),
  KEY `snmp_function_id` (`snmp_function_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=binary AUTO_INCREMENT=40 ;

--
-- Dumping data for table `snmp_queries`
--

INSERT INTO `snmp_queries` (`_id`, `device_id`, `snmp_function_id`, `interval`) VALUES
(12, 8, 1, 600),
(13, 3, 1, 600),
(38, 3, 2, 60),
(39, 8, 2, 60);

-- --------------------------------------------------------

--
-- Table structure for table `snmp_storage`
--

CREATE TABLE IF NOT EXISTS `snmp_storage` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `query_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`_id`),
  KEY `template_id` (`query_id`)
) ENGINE=InnoDB DEFAULT CHARSET=binary AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `snmp_storage_binary`
--

CREATE TABLE IF NOT EXISTS `snmp_storage_binary` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL,
  `extra` varchar(255) NOT NULL,
  `result` varbinary(4096) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `storage_id` (`storage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `snmp_storage_err`
--

CREATE TABLE IF NOT EXISTS `snmp_storage_err` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL,
  `extra` varchar(255) NOT NULL,
  `result` varchar(4096) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `storage_id` (`storage_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `snmp_storage_int`
--

CREATE TABLE IF NOT EXISTS `snmp_storage_int` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL,
  `extra` varchar(255) NOT NULL,
  `result` int(11) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `storage_id` (`storage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=198801 ;

-- --------------------------------------------------------

--
-- Table structure for table `snmp_storage_string`
--

CREATE TABLE IF NOT EXISTS `snmp_storage_string` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_id` int(11) NOT NULL,
  `extra` varchar(255) NOT NULL,
  `result` varchar(4096) NOT NULL,
  PRIMARY KEY (`_id`),
  KEY `template_id` (`storage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=121 ;

-- --------------------------------------------------------

--
-- Table structure for table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `templates`
--

INSERT INTO `templates` (`_id`, `name`, `image`) VALUES
(1, 'General device', 'pic/switch.png'),
(2, 'Cisco 2950', 'pic/switch.png');

-- --------------------------------------------------------

--
-- Table structure for table `templates_snmp`
--

CREATE TABLE IF NOT EXISTS `templates_snmp` (
  `_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `snmp_function_id` int(11) NOT NULL,
  `interval` int(11) NOT NULL DEFAULT '0',
  `is_hex` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_id`),
  KEY `template` (`template_id`),
  KEY `snmp_function_id` (`snmp_function_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=binary AUTO_INCREMENT=5 ;

--
-- Dumping data for table `templates_snmp`
--

INSERT INTO `templates_snmp` (`_id`, `template_id`, `snmp_function_id`, `interval`, `is_hex`) VALUES
(1, 2, 1, 600, 0),
(4, 2, 2, 60, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `map`
--
ALTER TABLE `map`
  ADD CONSTRAINT `map_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`_id`) ON DELETE CASCADE;

--
-- Constraints for table `map_links`
--
ALTER TABLE `map_links`
  ADD CONSTRAINT `map_links_ibfk_1` FOREIGN KEY (`one`) REFERENCES `devices` (`_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `map_links_ibfk_2` FOREIGN KEY (`two`) REFERENCES `devices` (`_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `map_links_ibfk_7` FOREIGN KEY (`one_query`) REFERENCES `snmp_functions` (`_id`),
  ADD CONSTRAINT `map_links_ibfk_8` FOREIGN KEY (`two_query`) REFERENCES `snmp_functions` (`_id`);

--
-- Constraints for table `snmp_queries`
--
ALTER TABLE `snmp_queries`
  ADD CONSTRAINT `snmp_queries_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `devices` (`_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `snmp_queries_ibfk_2` FOREIGN KEY (`snmp_function_id`) REFERENCES `snmp_functions` (`_id`);

--
-- Constraints for table `snmp_storage`
--
ALTER TABLE `snmp_storage`
  ADD CONSTRAINT `snmp_storage_ibfk_1` FOREIGN KEY (`query_id`) REFERENCES `snmp_queries` (`_id`) ON DELETE CASCADE;

--
-- Constraints for table `snmp_storage_binary`
--
ALTER TABLE `snmp_storage_binary`
  ADD CONSTRAINT `snmp_storage_binary_ibfk_1` FOREIGN KEY (`storage_id`) REFERENCES `snmp_storage` (`_id`) ON DELETE CASCADE;

--
-- Constraints for table `snmp_storage_err`
--
ALTER TABLE `snmp_storage_err`
  ADD CONSTRAINT `snmp_storage_err_ibfk_1` FOREIGN KEY (`storage_id`) REFERENCES `snmp_storage` (`_id`) ON DELETE CASCADE;

--
-- Constraints for table `snmp_storage_int`
--
ALTER TABLE `snmp_storage_int`
  ADD CONSTRAINT `snmp_storage_int_ibfk_1` FOREIGN KEY (`storage_id`) REFERENCES `snmp_storage` (`_id`) ON DELETE CASCADE;

--
-- Constraints for table `snmp_storage_string`
--
ALTER TABLE `snmp_storage_string`
  ADD CONSTRAINT `snmp_storage_string_ibfk_1` FOREIGN KEY (`storage_id`) REFERENCES `snmp_storage` (`_id`) ON DELETE CASCADE;

--
-- Constraints for table `templates_snmp`
--
ALTER TABLE `templates_snmp`
  ADD CONSTRAINT `templates_snmp_ibfk_2` FOREIGN KEY (`snmp_function_id`) REFERENCES `snmp_functions` (`_id`),
  ADD CONSTRAINT `templates_snmp_ibfk_1` FOREIGN KEY (`template_id`) REFERENCES `templates` (`_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
