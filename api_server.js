/**
 * Waffles API Server
 * mainnika, 2013
 */

var http = require('http'),
	async = require('async'),
	forms = require('formidable'),
	url = require('url'),
	log = require('./logger.js').getLogger('HTTP');

function ApiServer(){

	var self = this;

	this._version = '0.56';

	this._func = {
		'version':function(param,callback){callback({server:self._version,node:process.version,platform:process.platform,arch:process.arch,uptime:process.uptime(),loadavg:require('os').loadavg()})},
		'test':function(param,callback){callback({'result':param})}
	};
	this._http = null;
	this._db = null;

}

ApiServer.prototype = (function(){

	return {
		getVersion: function(){
			return this._version;
		},
		getEventer: function(){
			var self = this;
			return function(){ // IT'S MAGIC TIME!!
				return function(req,res){
					log.info('Http connection received: '+req.url+'; from '+(req.headers['x-real-ip'] || req.connection.remoteAddress));

					var _body = {},
						counter = 0,
						path = url.parse(req.url,true),
						self = this;

					function oSize(obj) {
						var size = 0, key;
						for (key in obj)
							if (obj.hasOwnProperty(key)) size++;
						return size;
					};

					function responser(body,status){
						log.info('Sending responce: '+status);
						var json = JSON.stringify(body);
						res.writeHead(status, {
						  'Access-Control-Allow-Origin': '*',
						  'X-Powered-By': 'PHP/4.4.9',
						  'Server': 'Apache/1.3.1 (Windows NT)', // Its funny
						  'Content-Type': 'application/json' 
						});
						res.end(json);
					}

					function static_responser(data,type,status){
						log.info('Sending static responce: '+status);
						res.writeHead(status, {
						  'Access-Control-Allow-Origin': '*',
						  'X-Powered-By': 'PHP/4.4.9',
						  'Server': 'Apache/1.3.1 (Windows NT)', // Its funny
						  'Content-Type': type 
						});
						res.end(data);
					}
					
					if ((path.pathname == '/api')&&(oSize(path.query)>0)){
						var tasks = [],
							ret = {'__times__': {}};
						for (m in path.query)
							tasks.push({func: m, param: path.query[m]});
						async.map(tasks, function(item,callback){
							log.info('Receive api function: '+item.func);
							var __start = new Date();
							if (typeof self._func[item.func] != 'function'){
								ret[item.func] = 'undefined';
								callback(null,'undefined');
							}else{
								self._func[item.func](item.param,function(body){
									ret[item.func] = body;
									ret['__times__'][item.func] = ((new Date())-__start);
									callback(null,body);
								})
							}
						}
						, function(err,results){
							responser(ret,200);
						})
					}else if(path.pathname == '/static'){

						log.info('Receive static query');

						var tag = path.query.tag,
							key = path.query.key;

						if ((!tag)||(!key))
							return static_responser(undefined, undefined, 404);
						if (req.method == 'POST'){

							var form = new forms.IncomingForm();

							form.parse(req);

							form.onPart = function(part){
								var bufs = [];
								part.on('data', function(data){
									bufs.push(data);
								})
								part.on('end', function(){
									self._db.staticStore(tag, key, part.mime, Buffer.concat(bufs), function(err,result){
										responser((err)?{error: err}:{store_id: result}, 200);
									});
								})
							}
						}else{

							self._db.staticGet(tag,key,function(err,result){
								if (err)
									return responser({error: err}, 200);
								if (!result)
									return responser({error: 'no suck store'}, 404);
								static_responser(result.value, result.type, 200);
							})

						}

					}else{
						responser({error:'what the fuck?'},403);
					}

				}.apply(self, arguments);
			}
		},

		pushFunc: function(cmd, func){
			log.info('Append function "'+cmd+'" to api')
			if (this._func[cmd] === undefined){
				this._func[cmd] = func;
			}
		},
		close: function(){
			log.info('Destriying api server');
			this._http.close();
		}
	}

})();

exports.getApiServer = function (port, callback){

	log.info('Creating api server...');
	var api_server = new ApiServer();

	api_server._http = http.createServer(api_server.getEventer());

	async.parallel({
		'api': function(callback){
			api_server._http.listen(port ,function(e){
				log.info('Api Server is '+((e)?('crashed: '+e):'created'));
				callback(e);
			})
		},
		'db': function(callback){
			log.info('Getting DB for ApiServer');
			require('./sql_storage.js').getStorage(function(err,storage){
				log.info('DB Storage '+((err)?err:storage));
				callback((err)?err:null,(err)?null:storage);
			})
		}
	}, function(err,result){
		if (err)
			return callback(err);
		api_server._db = result.db;
		callback(null,api_server);
	})

}


